
let oilPrice = obj.oilPrice;
let workPrice = obj.workPrice;
let koefPrice = obj.koefPrice;


let oilPriceUkr = obj.oilPriceUkr;
let oilPriceInt = obj.oilPriceInt;
let oilPrice3 = obj.oilPrice3;
let oilPrice4 = obj.oilPrice4;
let oilPrice5 = obj.oilPrice5;


let filterPrice = obj.filterPrice;
let filterPrice2 = obj.filterPrice2;
let filterPrice3 = obj.filterPrice3;
let filterPrice4 = obj.filterPrice4;



/*let oilPrice = 0; // НЕ ТРОГАТЬ
let filterPrice = 200; // НЕ ТРОГАТЬ

const oilPriceUkr = 200; // СТОИМОСТЬ ОТЕЧЕСТВЕННОГО МАСЛА
const oilPriceInt = 700; // СТОИМОСТЬ ИМПОРТНОГО МАСЛА
const workPrice = 600; // СТОИМОСТЬ РАБОТ
const koefPrice = 1.2; // КОЭФИЦИЕНТ ПОВЫШЕНИЯ ОБЩЕЙ СТОИМОСТИ. Если 1 - то цена не увеличивается*/


load_json_data('country');

function load_json_data(id, parent_id)
{
    let html_code = '';
    $.getJSON('./data/cars.json', function(data){
        let idRus = id;
        if(id === 'country') {
            idRus = 'Марка'
        }
        if(id === 'state') {
            idRus = 'Модель'
        }
        if(id === 'city') {
            idRus = 'Двигатель'
        }
        if(id === 'year') {
            idRus = 'Год выпуска'
        }
        if(id === 'oil') {
            idRus = 'Масло'
        }

            html_code += '<option value="">'+ idRus +'</option>';
            $.each(data, function (key, value) {
                if (id === 'country' || id == ' ' )   {
                    if (value.parent_id === '0') {
                        html_code += '<option value="' + value.id + '">' + value.name + '</option>';
                        $('#state').prop('disabled', true);
                        $('#city').prop('disabled', true);
                        $('#year').prop('disabled', true);
                        $('#oil').prop('disabled', true);
                        $('.cost-to-second').css('display','none');
                    }
                }
                else {

                    if (value.parent_id === parent_id) {
                        html_code += '<option value="' + value.id + '">' + value.name + '</option>';
                        $('#state').prop('disabled', false);

                    }

                }
            });
            $('#' + id).html(html_code);

    });




    $(document).on('change', '#country', function(){
        var car_id = $(this).val();
        if(!car_id)
        {
            $('#state').html('<option value="">Модель</option>');
            $('#city').html('<option value="">Двигатель</option>');
            $('#year').html('<option value="">Год выпуска</option>');
            $('#oil').html('<option value="">Масло</option>');

            $('#state').prop('disabled', true);
            $('#city').prop('disabled', true);
            $('#year').prop('disabled', true);
            $('#oil').prop('disabled', true);
            $('#button').css('display','none')

        }
        else
        {

            load_json_data('state', car_id);

            $('#city').html('<option value="">Двигатель</option>');
            $('#year').html('<option value="">Год выпуска</option>');
            $('#oil').html('<option value="">Масло</option>');


            $('#city').prop('disabled', true);
            $('#year').prop('disabled', true);
            $('#oil').prop('disabled', true);

            $('#button').css('display','none')
            $('.result').css('display','none')
            $('#buttonNew').css('display','none');
            $('.cost-to-second').css('display','none');



        }
    });

    $(document).on('change', '#state', function(){
        var state_id = $(this).val();
        if(!state_id)
        {
            $('#city').html('<option value="">Двигатель</option>');
            $('#year').html('<option value="">Год выпуска</option>');
            $('#oil').html('<option value="">Масло</option>');


            $('#city').prop('disabled', true);
            $('#year').prop('disabled', true);
            $('#oil').prop('disabled', true);
            $('#button').css('display','none')
            $('.cost-to-second').css('display','none');
        }
        else
        {

            load_json_data('city', state_id);

            $('#city').prop('disabled', false);
            $('#year').prop('disabled', true);
            $('#oil').prop('disabled', true);

            $('#year').html('<option value="">Год выпуска</option>');
            $('#oil').html('<option value="">Масло</option>');

            $('#button').css('display','none')
            $('.result').css('display','none')
            $('#buttonNew').css('display','none');
            $('.cost-to-second').css('display','none');


        }
    });
    $(document).on('change', '#city', function(){
        var city_id = $(this).val();


        if(!city_id)
        {
            $('#year').html('<option value="">Год выпуска</option>');
            $('#oil').html('<option value="">Масло</option>');

            $('#year').prop('disabled', true);
            $('#oil').prop('disabled', true);
            $('#button').css('display','none')
            $('.cost-to-second').css('display','none');

        }
        else
        {

            load_json_data('year', city_id);

            $('#year').prop('disabled', true);
            $('#oil').prop('disabled', true);

            $('#year').prop('disabled', false);
            $('#oil').html('<option value="">Масло</option>');
            $('#button').css('display','none')
            $('.result').css('display','none')
            $('#buttonNew').css('display','none');
            $('.cost-to-second').css('display','none');


        }
    });


    $(document).on('change', '#year', function(){
        var year_id = $(this).val();

        if(year_id !== '')
        {
            load_json_data('oil', year_id);
            $('#oil').prop('disabled', false);
            $('#button').css('display','none')
            $('.cost-to-second').css('display','none');
        }
        else
        {
            $('#oil').html('<option value="">Масло</option>');
            $('#button').css('display','none')
            $('.result').css('display','none')
            $('#buttonNew').css('display','none');
            $('.cost-to-second').css('display','none');
        }
    });

    $(document).on('change', '#oil', function(){
        var oil_id = $(this).val();
        if((oil_id.substr(length-3)) === '11'){oilPrice = oilPriceUkr}
        else if ((oil_id.substr(length-3)) === '22'){oilPrice = oilPriceInt}
        else if ((oil_id.substr(length-3)) === '33'){oilPrice = oilPrice3}
        else if ((oil_id.substr(length-3)) === '44'){oilPrice = oilPrice4}
        else if ((oil_id.substr(length-3)) === '55'){oilPrice = oilPrice5}

        // else {oilPrice = oilPriceInt}
        if(oil_id !== '')
        {
            $('.cost-to-second').css('display','none');
            $('.result').css('display','none')
            $('#button').css('display','inline-block')
            $('#button').on('click', ()=>{
                let count = oilPrice*koefPrice + workPrice + filterPrice*koefPrice;
                $('#oilPrice').html(oilPrice*koefPrice.toFixed(2) + ' грн.');
                $('#workPrice').html(workPrice.toFixed(2) + ' грн.');
                $('#filterPrice').html(filterPrice*koefPrice.toFixed(2) + ' грн.');

                $('#itog').html(count.toLocaleString() + ' грн.');
                $('#itogRed').html(count.toLocaleString() + ' грн.');

                $('.result').css('display','inline-block')
                $('#button').css('display','none');
                $('#buttonNew').css('display','inline-block');
                $('.cost-to-second').css('display','inline-block');

                console.log()
            })
        }
        else
        {
            $('#oil').html('<option value="">Масло</option>');
            $('#button').css('display','none')
            $('.result').css('display','none')
            $('#buttonNew').css('display','none');
            $('.cost-to-second').css('display','none');
        }
    });



    oil_




}






